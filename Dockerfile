FROM debian:10
ARG DEBIAN_FRONTEND=noninteractive

ADD Qt5.12.8-linux-gcc_64.tar.gz /opt/Qt/5.12.8/
ENV LD_LIBRARY_PATH="/opt/Qt/5.12.8/gcc_64/lib:${LD_LIBRARY_PATH}"
ENV PATH="/opt/Qt/5.12.8/gcc_64/bin:${PATH}"

RUN apt-get update && apt-get install -y openssh-server \
git                   \            
build-essential       \
default-jdk           \    
libfontconfig1        \       
python3               \
python3-pip           \
libgl1-mesa-dev

RUN pip3 install pytest cbor

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone 

RUN useradd -rm -d /home/ubuntu -s /bin/bash -g root -G sudo -u 1000 user \
&& usermod -aG sudo user \
&& echo 'user:pass' | chpasswd

RUN service ssh start
EXPOSE 22

CMD ["/usr/sbin/sshd","-D"]
